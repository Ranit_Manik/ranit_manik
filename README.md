<img align="right" width="30%" src="https://github.com/RanitManik/ranitmanik/assets/138437760/a6c87d8d-440d-4dda-8f91-22d85e53262e">
  
# Hi👋, I'm Ranit Manik

<h3>𝑨 𝒑𝒂𝒔𝒔𝒊𝒐𝒏𝒂𝒕𝒆 𝒅𝒆𝒔𝒊𝒈𝒏𝒆𝒓 𝒂𝒏𝒅 𝒅𝒆𝒗𝒆𝒍𝒐𝒑𝒆𝒓 𝒇𝒓𝒐𝒎 𝑰𝒏𝒅𝒊𝒂</h3>

- 🏫 Currently pursuing a Bachelor's degree in Computer Science at [CEMK](https://cemkolaghat.in/).
- 💻 Open Source Contributor with a focus on [GitHub](https://github.com/RanitManik).
- 📚 Proficient in HTML, CSS, JavaScript, Python, Java, C, and Figma.
- 🌐 Visit my [Portfolio](https://github.com/RanitManik/Portfolio-1.0) to explore my projects and achievements.
- 📝 Writing technical blogs on [Hashnode](https://hashnode.com/@ranitmanik).
- 📧 Reach me via [Email](mailto:ranitmanik.dev@gmail.com) or connect on [LinkedIn](https://www.linkedin.com/in/ranit-manik/).
- 📍 Currently based in Mecheda, West Bengal, India.

<br>

<h2 align="center">🧑‍💻 𝗟𝗮𝗻𝗴𝘂𝗮𝗴𝗲 𝗦𝘁𝗮𝘁𝘀 🧑‍💻</h2>

<table width="100%" align="center">
  </tr>
  <tr>
    <td width="40%"  align="center">
        <a href="https://github.com/RanitManik">
          <picture>
            <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-stats.vercel.app/api/top-langs/?username=RanitManik&layout=compact&theme=radical&langs_count=10" />
            <source media="(prefers-color-scheme: light)" srcset="https://github-readme-stats.vercel.app/api/top-langs/?username=RanitManik&layout=compact&langs_count=10" />
            <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=RanitManik&layout=compact&theme=radical&langs_count=10" alt="Top Languages" />
          </picture>
        </a>
    </td>
    <td width="60%"  align="center">
        <a href="https://github.com/RanitManik">
          <picture>
            <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-stats.vercel.app/api/wakatime?username=RanitManik&layout=compact&theme=radical&langs_count=10" />
            <source media="(prefers-color-scheme: light)" srcset="https://github-readme-stats.vercel.app/api/wakatime?username=RanitManik&layout=compact&langs_count=10" />
            <img align="center" src="https://github-readme-stats.vercel.app/api/wakatime?username=RanitManik&layout=compact&theme=radical&langs_count=10" alt="Wakatime Stats" />
          </picture>
        </a>
      </td>
  </tr>
</table>


<h2 align="center">💻 𝗧𝗲𝗰𝗵 𝗦𝘁𝗮𝗰𝗸 💻</h2>

<div align="center">
  <table width="100%" align="center">
    <tr>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/JavaScript-logo.png/800px-JavaScript-logo.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1869px-Python-logo-notext.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://github.com/RanitManik/ranitmanik/assets/138437760/f45d54d7-a34e-427c-a63d-2933e6ed84d7"/>
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/1/19/C_Logo.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://github.com/RanitManik/ranitmanik/assets/138437760/22913f42-3041-4abc-bc4c-3f3ce3ed6525" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/CSS3_logo.svg/1024px-CSS3_logo.svg.png" />
      </td>
          <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/2560px-Bootstrap_logo.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://github.com/RanitManik/ranitmanik/assets/138437760/d4317004-4b42-442b-9ee7-492015917790" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png" />
      </td>
      </td>
    </tr>
    <tr>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Figma-logo.svg/1667px-Figma-logo.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://www.edigitalagency.com.au/wp-content/uploads/Canva-logo-png-circle-full-colour-white-font.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://github.com/RanitManik/ranitmanik/assets/138437760/50a7aebc-dca5-4693-907a-f0b46637a04f" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://github.com/RanitManik/ranitmanik/assets/138437760/5e63dbf6-bb00-4587-b157-156efd62574d" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Adobe_Illustrator_CC_icon.svg/2101px-Adobe_Illustrator_CC_icon.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Adobe_Premiere_Pro_CC_icon.svg/1200px-Adobe_Premiere_Pro_CC_icon.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Adobe_After_Effects_CC_icon.svg/180px-Adobe_After_Effects_CC_icon.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Blender_logo_no_text.svg/2503px-Blender_logo_no_text.svg.png" />
      </td>
      <td align="center">
          <img height="40" align="center" src="https://i.redd.it/tu3gt6ysfxq71.png" />
      </td>
    </tr>
  </table>
</div>

---

<div align="center">
  <img height="20" padding-left=20 src="https://wakatime.com/badge/user/6c66cc47-ce26-48cc-a555-22494865c546.svg" alt=""/>
  <img height="20" src="https://visitcount.itsvg.in/api?id=RanitManik&icon=0&color=0" alt=""/>
</div>
